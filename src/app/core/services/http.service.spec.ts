import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';

describe('HttpService', () => {
  //declarations
  let service: HttpService;
  let httpTestingController: HttpTestingController;
  //beforeEach method
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
     service = TestBed.inject(HttpService);
     httpTestingController = TestBed.inject(HttpTestingController);
  });

  //afterEach method
  afterEach(() => {
    httpTestingController.verify(); // to make sure there are no outstanding HTTP requests
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get data with the HttpTesting Controller', () => {
    service.getDummyData().subscribe(data => {
      expect(data).toEqual('achref');            //recieve the data returned by the service 
    });

    const req = httpTestingController.expectOne('fake/url');  //expect this url to be called only once in this test
    expect(req.request.method).toEqual('GET');  // expect the method to be GET
    req.flush('achre')                          // execute the request 
  });
});
