import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AddHelloService {

  constructor() { }

  addHello(input:string){
    return input + " Hello";
  }
}
