
import { Http2Service } from './http2.service';
import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

describe('Http2Service', () => {
  let service: Http2Service;
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Http2Service, {
        provide: HttpClient, useValue: httpClientSpy
      }]
    });
   // service = TestBed.inject(Http2Service);
   service = new Http2Service(httpClientSpy);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should should use the spy http to perfom the call', (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of('using method 2 to mock the spy'));
    service.getPosts().subscribe((data) => {
      expect(data).toEqual('using method 2 to mock the spy');
      done();
    });
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
  });


});
