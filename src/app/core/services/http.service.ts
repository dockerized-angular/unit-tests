import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  todos:string='';

  constructor(private httpClient:HttpClient) { }

  getDummyData():Observable<string>{
    return this.httpClient.get<string>('fake/url').pipe(map(val=> val+'f'));
  }
}
