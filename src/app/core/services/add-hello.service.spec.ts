import { TestBed } from '@angular/core/testing';

import { AddHelloService } from './add-hello.service';

describe('AddHelloService', () => {
  let service: AddHelloService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddHelloService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
