import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Http2Service {

  constructor(private httpClient:HttpClient) { }

  getPosts():Observable<string>{
    return this.httpClient.get<string>('');
  }
}
