import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { HttpService } from '../../../core/services/http.service';
import { of } from 'rxjs';
import { HighlighterDirective } from '../../directives/highlighter.directive';

describe('HeaderComponent', () => {
  let headerComponent: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  //let addHelloService: AddHelloService;  // ne need , have no nested dependency
  //let httpService: HttpService;
  const mockHttpService = jasmine.createSpyObj('HttpService', ['getDummyData']);

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [HeaderComponent, HighlighterDirective],
      //  imports: [HttpClientModule],
      providers: [
        //    AddHelloService,    //no need here since this service has no nested dependencies 
        { provide: HttpService, useValue: mockHttpService }  //must eiter add the mock or the real dependency
      ]
    })
      .compileComponents();
    fixture = TestBed.createComponent(HeaderComponent);
    //  addHelloService = TestBed.inject(AddHelloService);
    //httpService = TestBed.inject(HttpService);
    headerComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(headerComponent).toBeTruthy();
  });

  it('should getData', () => {
    mockHttpService.getDummyData.and.returnValue(of('achref'));
    headerComponent.fakeHttpCall();
    expect(mockHttpService.getDummyData).toHaveBeenCalled();
    expect(headerComponent.todos).toEqual('achref');
  });

  it('should set Greeting to user Hello', () => {
    expect(headerComponent.name).toEqual('achref');
    expect(headerComponent.greeting).toEqual('achref Hello');
  });

  it('set the user and the value in the template', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h1')?.textContent).toContain('achref Hello');
  });

  it('should create and return mocked data of the service method', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h1')?.textContent).toContain('achref Hello');
  });

  it('should set h2 tag to todos value', () => {
    mockHttpService.getDummyData.and.returnValue(of('achref'));
    // spyOn(httpService, 'getDummyData').and.returnValue(of('achref'));
    headerComponent.fakeHttpCall();
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h2')?.textContent).toEqual('achref');
  });

});
