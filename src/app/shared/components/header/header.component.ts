import { Component, OnInit } from '@angular/core';
import { AddHelloService } from '../../../core/services/add-hello.service';
import { HttpService } from '../../../core/services/http.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit{
  name :string = 'achref';
  greeting!:string;
  todos!:string;

  constructor(private service:AddHelloService,private httpService:HttpService){
  }
  ngOnInit(): void {
    this.greeting = this.service.addHello(this.name);
  }

  fakeHttpCall(){
    this.httpService.getDummyData().subscribe((data)=>{
      this.todos = data;
    })
  }

}
