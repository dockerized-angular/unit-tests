import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlighter]'
})
export class HighlighterDirective {


  @Input() mark!: number;
  @HostBinding('style.backgroundColor') backgroundColor!: string;

  constructor() { }

  @HostListener('mouseenter') onMouseEnter(){
    if(this.mark>20){
      this.backgroundColor = 'green'
    }else{
      this.backgroundColor = 'yellow';
    }
  }

  @HostListener('mouseleave') Onmouseleave(){
    this.backgroundColor = 'transparent';
  }
}
