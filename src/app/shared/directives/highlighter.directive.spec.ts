import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HighlighterDirective } from './highlighter.directive';
import { HeaderComponent } from '../components/header/header.component';
import { DebugElement } from '@angular/core';
import { AppComponent } from '../../app.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { By } from '@angular/platform-browser';

describe('HighlighterDirective', () => {
  let component!: HeaderComponent;
  let fixture:ComponentFixture<HeaderComponent>;
  let el:DebugElement;

  beforeEach(()=>{
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      declarations:[HeaderComponent,HighlighterDirective],
    })
  fixture = TestBed.createComponent(HeaderComponent);
  component = fixture.componentInstance;
  el = fixture.debugElement;
  fixture.detectChanges();

  })

  it('should create an instance', () => {
    const directive = new HighlighterDirective();
    expect(directive).toBeTruthy();
  });


  it('should turn attribute to green when moussenter and transparent when the mouse leave', () => {
    //const compiled = fixture.nativeElement as HTMLElement;
    const div0= el.queryAll(By.css('div'))[0];
    div0.triggerEventHandler('mouseenter',{});
    fixture.detectChanges();
    expect(div0?.nativeElement.style.backgroundColor).toBe('green');
    div0.triggerEventHandler('mouseleave',{});
    fixture.detectChanges();
    expect(div0?.nativeElement.style.backgroundColor).toBe('transparent');

  });

});
