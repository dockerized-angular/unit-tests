import { ReversePipe } from './reverse.pipe';

describe('ReversePipe', () => {
  const pipe = new ReversePipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return the reverse of the given input string', () => {
    expect(pipe.transform('amal')).toEqual('lama');
  })
});
