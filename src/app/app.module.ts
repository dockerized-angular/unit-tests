import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { ReversePipe } from './shared/pipes/reverse.pipe';
import { HighlighterDirective } from './shared/directives/highlighter.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ReversePipe,
    HighlighterDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
