FROM nginx:1.25.3

COPY dist/unit-tests/browser/ /usr/share/nginx/html 
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80